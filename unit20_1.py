# Функция скачивания изображений

import random
import urllib.request

names = []


def download_img(url):

    name = random.randrange(5)
    if name in names:
        name += 1000
        names.append(name)
        name = str(name) + ".jpg"
    else:
        names.append(name)
        name = str(name) + ".jpg"
    try:
        urllib.request.urlretrieve(url, name)
        print('картинка успешно загружена')
        print(names)
        print(name)
    except urllib.error.HTTPError:
        print('Нет такой картинки, дайте другой адрес')


download_img('https://udemy-images.udemy.com/course/750x422/507888_17b7_2.jpg')

# 1 Проверка на неповторимость имен
# 2 При ошибке с адресом картинки брать новую или останавливать программу (try except)
# https://pythonworld.ru/tipy-dannyx-v-python/isklyucheniya-v-python-konstrukciya-try-except-dlya-obrabotki-isklyuchenij.html