num1 = float(input("Первое число: "))
num2 = float(input("Второе число: "))
operation = input("Что делаем? (+ - * /) :")


def calc(num1, num2):
    global res
    if operation == '+':
        res = num1 + num2
    elif operation == '-':
        res = num1 - num2
    elif operation == '*':
        res = num1 * num2
    elif operation == '/':
        if num2 == 0:
            print('Деление на ноль!!!')
            return 0

        else:
            res = num1 / num2
    else:
        print('Недопустимая операция!')
        return 0
    return res


print(calc(num1, num2))


