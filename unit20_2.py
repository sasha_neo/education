# Функция скачивания изображений
# Имя файла формируется не рандомно, а в виде даты-времени

import urllib.request
import datetime


def download_img(url):

    name = datetime.datetime.today().strftime("%Y-%m-%d-%H.%M.%S")
    name = str(name) + ".jpg"
    try:
        urllib.request.urlretrieve(url, name)
        print('картинка успешно загружена')
        print(name)
    except urllib.error.HTTPError:
        print('Нет такой картинки, дайте другой адрес')


download_img('https://udemy-images.udemy.com/course/750x422/507888_17b7_2.jpg')

# 1 Проверка на неповторимость имен
# 2 При ошибке с адресом картинки брать новую или останавливать программу (try except)
# https://pythonworld.ru/tipy-dannyx-v-python/isklyucheniya-v-python-konstrukciya-try-except-dlya-obrabotki-isklyuchenij.html
