def worker(*nums):
    mult = 1
    quantity = len(nums)
    for f in nums:
        mult *= f
    return mult, quantity


print(worker(1, 2, 3, 4, 5, 6, 7))

(5040, 7)