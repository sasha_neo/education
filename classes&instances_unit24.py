# Classes and instances


class Car():
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        self.odometer_reading = 0

    def description_mame(self):
        desc = str(self.year) + ' ' + self.make + self.model
        return desc

    def read_odometer(self):
        """Выводим пробег авто"""
        print("Пробег этого авто " + str(self.odometer_reading) + ' km')

    def update_odometer(self, km):
        """Устанавливает значение одометра"""
        if km >= self.odometer_reading:
            self.odometer_reading = km
        else:
            print('Не стоит так делать!')

    def increment_odometer(self, km):
        """Увеличиваем показания одометра на заданную величину"""
        if km >= 0:
            self.odometer_reading += km
        else:
            print('Увеличить можно только положительным пробегом')


my_car = Car('Audi', 'A4', 2017)

print(my_car.description_mame())
my_car.update_odometer(30)
my_car.increment_odometer(670)
my_car.read_odometer()

