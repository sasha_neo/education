# Creating classes


class Restaurant():

    def __init__(self, restaurant_name, cuisine_type):
        """creation class Restaurant"""
        self.restaurant_name = restaurant_name
        self.cuisine_type = cuisine_type

    def describe_restaurant(self):
        print(self.restaurant_name + ", " + self.cuisine_type)

    def open_restaurant(self):
        print(self.restaurant_name + ' is OPEN')


class User():
    def __init__(self, first_name, last_name, age, sex):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.sex = sex

    def describe_user(self):
        print("Даныые о пользователе => " + self.first_name, self.last_name, self.age, self.sex)

    def greet_user(self):
        print(self.first_name, self.last_name + ', Добро пожаловать!!!')


soldier = User('Валентин', 'Пуговкин', 23, 'мужчина')
pretty_girl = User('Елизаветта', 'Охтырская', 25, 'женщина')
mechanic = User('Юрий', 'Лей', 36, 'мужчина')

pretty_girl.describe_user()
pretty_girl.greet_user()

mechanic.greet_user()

restaurant = Restaurant("Yummy", "japan")

print(restaurant.restaurant_name)
print(restaurant.cuisine_type)
restaurant.describe_restaurant()
restaurant.open_restaurant()

rest1 = Restaurant('CHA-CHA', 'asian')
rest2 = Restaurant('Amore', 'french')
rest3 = Restaurant('Khata', 'ukrainian')

rest1.describe_restaurant()
rest2.describe_restaurant()
rest3.describe_restaurant()

