class Car():
    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        self.odometer_reading = 0

    def description_name(self):
        desc = str(self.year) + ' ' + self.make + ' ' + self.model
        return desc

    def read_odometer(self):
        """Выводим пробег авто"""
        print("Пробег этого авто " + str(self.odometer_reading) + ' km')

    def update_odometer(self, km):
        """Устанавливает значение одометра"""
        if km >= self.odometer_reading:
            self.odometer_reading = km
        else:
            print('Не стоит так делать!')

    def increment_odometer(self, km):
        """Увеличиваем показания одометра на заданную величину"""
        if km >= 0:
            self.odometer_reading += km
        else:
            print('Увеличить можно только положительным пробегом')


class Battery():
    """Простая модель аккумулятора для автомобиля"""
    def __init__(self, battery_left = 0, battery = 100):
        self.battery = battery
        self.battery_left = battery_left

    def description_battery(self):
        """Выводит информацию о мощности батареи"""
        print('Этот автомобиль имеет батарею мощностью ' + str(self.battery) + ' кВт')

    def battery_discharge(self, km):
        """метод разряда батареи, который принимает параметр пробега
         и внутри умножает пробега на расход батареии и
          потом отнимает данные с атрибута батареи"""
        battery_left = self.battery - (km * 0.2)
        if battery_left <= 0:
            print('Батарея разряжена')
        else:
            print('Остаток батареи после пробега ' + str(km) + ' km составил ' + str(battery_left))

    def battery_charge(self, charging_time):
        """метод зарядки батареи до 100% значения"""
        battery_charge = charging_time * 10 + self.battery_left
        print('Уровень заряда батареи после ' + str(charging_time) + '-часовой зарядки составил ' + str(battery_charge))


class ElectricCar(Car):
    """ Аспекты для электромобиля"""
    def __init__(self, make, model, year):
        """Инициализация атрибутов класса родителя"""
        super(). __init__(make, model, year)
        self.battery = Battery()

    def description_mame(self):
        """Переопределение родительского метода"""
        desc = str(self.year) + ' ' + self.model
        return desc

