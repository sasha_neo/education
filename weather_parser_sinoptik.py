# https://sinoptik.ua/погода-харьков

import requests
from bs4 import BeautifulSoup
features = "html.parser"
url = 'https://sinoptik.ua/погода-харьков'

my_request = requests.get(url)
main_text = my_request.text
soup = BeautifulSoup(main_text, features="html.parser")
weath_box = soup.findAll({'span': 'main loaded'})
temp_min = weath_box[1].text
temp_max = weath_box[2].text
desc = soup.findAll('', {'class': 'description'})
desc = desc[0].text
temp_now = soup.findAll('p', {'class': 'today-temp'})
print('----------------------------------------')
print('В Харькове сегодня ' + temp_min + '..' + temp_max)
print('Сейчас температура воздуха ' + temp_now[0].text)
print(desc)


