# https://minfin.com.ua/currency/nbu/

import requests
from bs4 import BeautifulSoup

url = 'https://minfin.com.ua/currency/nbu/'

source = requests.get(url)
main_text = source.text
soup = BeautifulSoup(main_text, features="html.parser")

table = soup.find('table', {'class': 'table-auto'})
tr = table.findAll('td', {'class': 'responsive-hide'})
usd = tr[0]
eur = tr[1]
rub = tr[2]
usd = usd.text
eur = eur.text
rub = rub.text
print('')
print('*****Курс валют НБУ: *****')
print('USD - ' + usd[1:7])
print('EUR - ' + eur[1:7])
print('RUB - ' + rub[1:7])
print('***************************')





